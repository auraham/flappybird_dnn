# test_dnn.py
from __future__ import print_function
from keras import models
from keras import layers
from keras.models import load_model
from dnn import load_dataset
import numpy as np

if __name__ == "__main__":
    
    # load model
    model = load_model("model_dnn.h5")
    
    # load dataset
    X_train, y_train, X_test, y_test, X_val, y_val = load_dataset()
    
    # predict some actions
    predictions = model.predict(X_test)
    
    # reshape and cast using threshold
    pred = predictions.flatten()
    mask = pred > 0.5
    pred[mask]  = 1
    pred[~mask] = 0
    
    # check accuracy
    n = len(y_test)
    error = (y_test != pred).sum() / n
    print("missclassified %.4f" % error)
